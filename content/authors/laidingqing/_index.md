---
# Display name
title: AsOne

# Is this the primary user of the site?
superuser: true

# Role/position
role: CTO/架构师/讲师

# Organizations/Affiliations
organizations:
  - name: KNDEV
    url: ''

# Short bio (displayed in user profile at end of posts)
bio: 我的兴趣与工作 - Java/Golang/Kotlin/Python/Rust、数据分析、敏捷开发。


# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: 'mailto:laidingqing@knmdev.com'
  - icon: github
    icon_pack: fab
    link: https://github.com/laidingqing

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: 'laidingqing@knmdev.com'

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
  - Teachers
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed neque elit, tristique placerat feugiat ac, facilisis vitae arcu. Proin eget egestas augue. Praesent ut sem nec arcu pellentesque aliquet. Duis dapibus diam vel metus tempus vulputate.
