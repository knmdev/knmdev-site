---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Knmdev.com - 提供云原生微服务开发系列课程"
subtitle: ""
summary: ""
authors: ["laidingqing"]
tags: []
categories: []
date: 2023-01-09T11:15:23+08:00
lastmod: 2023-01-09T11:15:23+08:00
featured: true
draft: false
share: false
# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

2023, knmdev.com开始对外提供云原生公开课及VIP系列课程。