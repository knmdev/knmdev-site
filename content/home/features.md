---
widget: featurette
headless: true
weight: 20
title: 课程特点
subtitle: ✨ 由浅入深，按需选择
feature:
  - icon: chalkboard-teacher
    icon_pack: fas
    name: 课件
    description:
  - icon: video
    icon_pack: fas
    name: 视频
    description:
  - icon: code
    icon_pack: fas
    name: 代码
    description: 
---
