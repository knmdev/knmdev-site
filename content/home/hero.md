---
widget: hero
headless: true
weight: 10
title: 云原生微服务开发
hero_media: teacher.png
design:
  background:
    gradient_angle: 0
    gradient_start: 'rgb(224,223,255)'
    gradient_end: 'rgb(153,238,255)'
    text_color_light: false
cta_alt:
  url:
  label:
cta_note:
  label:
advanced:
  css_class: fullscreen
---

<br>

Knmdev.com是您开始成为云原生开发者的理想起点.
您将通过实践学习云原生生态中各部份的技能.

你将学习:

* 现代语言基础与进阶，如Java/Kotlin/Golang
* 在容器与云平台中编排应用程序
* 使用框架Quarkus/Gin等实现微服务应用程序
* 实现持续交付的方法
* 观测微服务应用与云平台